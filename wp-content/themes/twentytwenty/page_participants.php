<?php
/**
 * Participants
 *
 * This file adds the landing page template to the Participants Theme.
 *
 * Template Name: ParticipantsPage
 *
 **/

 ?>
<h1>Participants</h1>
<table>
<thead>
    <tr>
       <th>Nom du participant</th>
	   <th>Courriel du participant</th>
	</tr>
</thead>
<tbody>
    <?php
		$the_query = new WP_Query(array("post_type" => "Participants", "orderby" => "date", "order" => "ASC"));
	?>
	<?php if ($the_query->have_posts()) { ?>
	<?php
		while ($the_query->have_posts()) {
		$the_query->the_post();
	?>
	<?php 
		$nom_de_participant = get_field('nom_de_participant');
		$courriel_de_participant = get_field('courriel_de_participant');
	?>
     <tr>
       <td><?php echo $nom_de_participant ?></td>
	   <td><?php echo $courriel_de_participant ?></td>
	 </tr>
	<?php
	}
	wp_reset_postdata();
	?>
	<?php } ?>	
</tbody>
</table>
<?php



