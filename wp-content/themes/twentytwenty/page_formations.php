<?php
/**
 * Formation
 *
 * This file adds the landing page template to the Formation Theme.
 *
 * Template Name: FormationPage
 *
 **/

 ?>
<h1>Formations</h1>
<table>
<thead>
    <tr>
       <th>Nom</th>
	   <th>Date de la formation</th>
	   <th>Date de la création</th>
	</tr>
</thead>
<tbody>
    <?php
		$the_query = new WP_Query(array("post_type" => "Formations", "orderby" => "date", "order" => "ASC"));
	?>
	<?php if ($the_query->have_posts()) { ?>
	<?php
		while ($the_query->have_posts()) {
		$the_query->the_post();
	?>
	<?php 
		$nom_de_la_formation = get_field('nom_de_la_formation');
		$date_de_la_formation = get_field('date_de_la_formation');
		$date_de_la_creation = get_field('date_de_la_formation');
	?>
     <tr>
       <td><?php echo $nom_de_la_formation ?></td>
	   <td><?php echo $date_de_la_formation ?></td>
	   <td><?php echo $date_de_la_creation ?></td>
	 </tr>
	<?php
	}
	wp_reset_postdata();
	?>
	<?php } ?>	
</tbody>
</table>
<?php


