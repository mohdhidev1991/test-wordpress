<?php
/*******post type Formations************/

add_action('init', 'Formations');
function Formations()
{

    $labels = array(
        'name' => _x('Formations', 'post type general name'),
        'singular_name' => _x('Formations', 'post type singular name'),
        'add_new' => _x('Add New', 'Formations item'),
        'add_new_item' => __('Add New item Formations'),
        'edit_item' => __('Edit Formations Item'),
        'new_item' => __('New Formations Item'),
        'view_item' => __('View Formations Item'),
        'search_items' => __('Search Formations'),
        'not_found' => __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields'),
        'rewrite' => array('slug' => 'Formations', 'with_front' => FALSE),
        'taxonomies' => array('category')
    );

    register_post_type('Formations', $args);
}

/*******post type Participants************/

add_action('init', 'Participants');
function Participants()
{

    $labels = array(
        'name' => _x('Participants', 'post type general name'),
        'singular_name' => _x('Participants', 'post type singular name'),
        'add_new' => _x('Add New', 'Participants item'),
        'add_new_item' => __('Add New item Participants'),
        'edit_item' => __('Edit Participants Item'),
        'new_item' => __('New Participants Item'),
        'view_item' => __('View Participants Item'),
        'search_items' => __('Search Participants'),
        'not_found' => __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields'),
        'rewrite' => array('slug' => 'Participants', 'with_front' => FALSE),
        'taxonomies' => array('category')
    );

    register_post_type('Participants', $args);
}









